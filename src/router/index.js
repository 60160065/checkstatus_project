import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'checkstatus',
    component: () => import(/* webpackChunkName: "about" */ '../views/checkStatus.vue')
  },
  {
    path: '/checkgrade',
    name: 'checkgrade',
    component: () => import(/* webpackChunkName: "about" */ '../views/checkGrade.vue')
  },
  {
    path: '/checkhour',
    name: 'checkhour',
    component: () => import(/* webpackChunkName: "about" */ '../views/checkHour.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
